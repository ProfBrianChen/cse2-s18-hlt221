/* Hayden Trautmann
hw10
4/23/18 
 This homework gives you practice with multidimensional arrays */


import java.util.Scanner;
public class RobotCity{
  
  public static void main(String [] args){
   int a = 0;
    do{
      
  int height = (int) (Math.random() * 10 + 5);
  int width = (int) (Math.random() * 10 + 5);
    
    //assigns the population of the city
    int[][] buildCity1 = new int[width][height];
      buildCity1 = buildCity(width, height);
    
    //method that prints out build city
    display(buildCity1);
    int k = 0;
    do{
    k = (int) (Math.random() * 899 + 1);
    }while( k > width * height);
  
    System.out.println( " ");
     System.out.println( " ");
    
    
    int[][] newCity = new int[width][height];
    // changes a certain value to negative in the array buildCity
    
    newCity = invade(buildCity1, k, width, height);
    
    display(newCity);
    
    a++;
    
    }while(a <5);
    
  }

  public static int[][] buildCity(int height, int width){
  
    int[][] buildCity = new int[width][height];
   int w = 0;
    for(int k=0; k<width; k++){ 
    for(int f = 0; f<height; f++){
      int num = (int) (Math.random() * 899 + 100);
      buildCity[k][f] = num;
      
      }
    }
  return buildCity;
  }

 public static void display(int[][] buildCity){
  //row major print
      for(int k=0; k < buildCity.length; k++){ 
      for(int f = 0; f< buildCity[k].length; f++){
      System.out.print(buildCity[k][f] + " ");
         }
        System.out.println();
      }
   }
  
  // lands robots on a certain part of hte multi dimensional array, changing the value to negative
  public static int[][] invade(int[][] buildCity, int k, int width, int height){
    
    int[][] newCity = new int[width][height];
   int w = 0;
   int h = 0;
    for(int i = 0; i < k; i++){
     
      // assign a location for the invader
       //checks to make sure it is within the range of the array, and, if not, re assigns values
      int t = 9;
      do{
      w = (int) (Math.random() * width +1);
      h = (int) (Math.random() * height +1); 
       // if( w < width && h < height){
         //    break ;
       // }
        //else{
          //continue;
        //}
      }while(w >= width && h >= height);
      
         
     for(int d=0; d < buildCity.length; d++){ 
         for(int f = 0; f < buildCity[0].length; f++){
           
            if(buildCity[d][f] == buildCity[w][h]){
                 buildCity[d][f] = (-1 * buildCity[d][f]);  //swaps the value to negative
            }
         }
       }
     }
          return buildCity;
    }

  }
      
      
      
     /* for(int d=0; d < buildCity.length; d++){ 
         for(int f = 0; f < buildCity[d].length; f++){
            if(buildCity[w][h] < 0){ //checks for overlapping invaders
             break;
             }
           else if(buildCity[d][f] == buildCity[w][h]){
                 int num = buildCity[d][f];
                 newCity[d][f] = (num * ( - 1));   //swaps the value to negative
            }
          
           else{
             newCity[d][f] = buildCity[d][f];
              }
                
           }
       
         }
        
     }  */ 
    
   
 


