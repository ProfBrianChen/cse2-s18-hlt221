//// Hayden Trautmann
// 3/25/18
// CSE2 hw07

import java.util.Scanner;

public class Area{
  
  public static void rectangle(double length, double width){
    double area = length * width;
    System.out.println("the area is " + area);
  }
  
  public static void triangle(double base, double height){
    double area = (base * height * .5);
     System.out.println("the area is " + area);
  }
  
  public static void circle(double radius){ 
    double area = radius * radius * 3.14159;
      System.out.println("the area is " + area);   
  }
  
  
  public static double read(){//if reads a double, return a double; if other input, ask again.
    double number = 0;
    String junk;
    Scanner scan = new Scanner(System.in);
    boolean input = scan.hasNextDouble();
   if(input == true){ 
 number = scan.nextDouble();
 }
 else{
   junk = scan.next();
   while( input == false){
     System.out.println("Error: enter double type ");
 input = scan.hasNextDouble();  
      if( input == true){
 number = scan.nextDouble();
        break;
      }
   }
}
  
    return number;
    }
  
  public static void main(String[] args){
    
   //main method, asks for input, checks if string, switch statement for what the shape is 
    Scanner scan = new Scanner(System.in);
    while(true){
      System.out.println("Enter rectangle, triangle, or circle");
      boolean entered = scan.hasNext();
      if(entered == true){
       String input = scan.next();
        switch (input){
          case "rectangle":
            double length;
            double width;
            System.out.println("Enter the length");
            length = read();
            System.out.println("Enter the width");
            width = read();
            rectangle(length, width);
            break;
          case "triangle":
            double base;
            double height;
            System.out.println("Enter the base");
            base = read();
            System.out.println("Enter the height");
            height = read();
            triangle(base, height);
              break;
          case "circle":
            double radius;
            System.out.println("Enter the radius");
            radius = read();
            circle(radius);
              break;
          default:
            System.out.println("Error: Must enter \"rectangle\", \"triangle\", or \"circle\" ");
           
        }
        
      }
      else{
        System.out.println("error must enter a string");
        String trash = scan.next();
     
      }
    }
  }
      
    }
 