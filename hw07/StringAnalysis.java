//// Hayden Trautmann
// 3/25/18
// CSE2 hw07

import java.util.Scanner;

public class StringAnalysis {

	public static boolean analyze(String str) {
		for (int i = 0; i <= str.length(); i++) {
			// leave because theres a non character
			if (!Character.isLetter(str.charAt(i))) {
				return false;
			}
		}
		return true;
	} //end of method analyze

	public static boolean analyze(String str, int count) {
	int length = 0;

		if (count > str.length()) {
			length = str.length();
		}
		else {
			length = count;
		}

		for (int i = 0; i < length; i++) {
			// leave because theres a non character
			if (!Character.isLetter(str.charAt(i))) {
				return false;
			}
		}

		return true;
	} //end of method analyze
	
	
//main method, user enters string, stores as string scanner, enter 1 for whole or 2 for certian number, run according method.
	public static void main(String[]args){
while(true){
		System.out.println("Enter a string");
      Scanner scan = new Scanner(System.in); 
    String str = scan.next();
	
		System.out.println("Enter 1 for analyzing all the characters in the string or enter 2 to analyze a certain number");
	int num = scan.nextInt();
	
		//determines which method to use
		
		if (num == 1){
			analyze(str);
			if(true){
					System.out.println("the string consists of all letters");
			}
			else{
					System.out.println("the string does not consist of all letters");
			}
			break; //exits while loop
		}
		else if (num == 2){
			System.out.println("Enter the number of integers you want to analyze");
			int count = scan.nextInt();
				analyze(str, count); //returns boolean based of in the string is letters or not
			
			if(true){
				System.out.println("All of the selected string consists of letters");
			}
			else{
				System.out.println("Not all of the selected string consists of letters");
			}
			break;
		}
		else{
			System.out.println("Error: must enter 1 or 2");
		}
		} // end of while loop
	}//end of main method
		
} //end of class