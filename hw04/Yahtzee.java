//// Hayden Trautmann
// 2/16/18
// CSE2 hw04
//he classic dice game, Yahtzee, involves rolling five dice and scoring the result of the roll.  
//Your program, called Yahtzee.java, is to perform a random roll of the dice and score the roll.  
//Since variations exist in the way Yahtzee is played, you are to compute the scores as described below 
//(See also the attached score card for a quick reference) and print out the following values: upper section initial total, 
//upper section total including bonus, lower section total, and grand total. The grand total is the sum of the upper section total 
//including bonus and the lower section total

import java.util.Scanner; //imports the scanner 
public class Yahtzee{
  
    public static void main(String[] args) { 
     //initializes the dies
      int dice1 = 0;
      int dice2 = 0;
      int dice3 = 0;
      int dice4 = 0;
      int dice5 = 0;
      System.out.println("If you want to enter a 5 digit number enter 1, if not, enter 2");
           Scanner scan = new Scanner(System.in);
    
      //creates a scanner for manually entering the 5 digits
      int number = scan.nextInt();
     if (number == 1){
       System.out.println("Enter a 5 digit number, pressing the return key after each single digit is entered: ");
       dice1 = scan.nextInt();
       dice2 = scan.nextInt();
       dice3 = scan.nextInt();
       dice4 = scan.nextInt();
       dice5 = scan.nextInt();

     }
      //randomly generates the 5 die rolls
      if (number == 2){
        dice1 = (int) Math.random() * 5 + 1;
        dice2 = (int) Math.random() * 5 + 1;
        dice3 = (int) Math.random() * 5 + 1;
        dice4 = (int) Math.random() * 5 + 1;
        dice5 = (int) Math.random() * 5 + 1;
       
      }
      
      
     //initializes the counters for the die faces
      int count1 = 0;
      int count2 = 0;
      int count3 = 0;
      int count4 = 0;
      int count5 = 0;
      int count6 = 0;
      
      //switch statement for each die that increments each die face 
     
      switch (dice1){
      case 1: 
      count1 ++;
        break;
      case 2: 
      count2 ++;
        break;
      case 3: 
      count3 ++;
        break;
      case 4: 
      count4 ++;
        break;
      case 5: 
      count5 ++;
        break;
      case 6: 
      count6 ++;
        break;
      }
         
     switch (dice2){
      case 1: 
      count1 ++;
        break;
      case 2: 
      count2 ++;
        break;
      case 3: 
      count3 ++;
        break;
      case 4: 
      count4 ++;
        break;
      case 5: 
      count5 ++;
        break;
      case 6: 
      count6 ++;
        break;
     }
             
      switch (dice3){
      case 1: 
      count1 ++;
        break;
      case 2: 
      count2 ++;
        break;
      case 3: 
      count3 ++;
        break;
      case 4: 
      count4 ++;
        break;
      case 5: 
      count5 ++;
        break;
      case 6: 
      count6 ++;
        break;
      }
      switch (dice4){
      case 1: 
      count1 ++;
        break;
      case 2: 
      count2 ++;
        break;
      case 3: 
      count3 ++;
        break;
      case 4: 
      count4 ++;
        break;
      case 5: 
      count5 ++;
        break;
      case 6: 
      count6 ++;
        break;
      }
         
            switch (dice5){
      case 1: 
      count1 ++;
        break;
      case 2: 
      count2 ++;
        break;
      case 3: 
      count3 ++;
        break;
      case 4: 
      count4 ++;
        break;
      case 5: 
      count5 ++;
        break;
      case 6: 
      count6 ++;
        break;
      }
      
      
     //total sum of the die 
     int sumTop = 0;
     sumTop =  (dice1 + dice2 + dice3 + dice4 + dice5);
     
     
      
   //initializes top scores
      int topScore1 = 0;
      //checks for aces
      if (count1 == 3 || count1 == 4 || count1 == 5){
        topScore1 = (int) 3; 
      }
        //checks for twos
      if (count2 == 3 || count2 == 4 || count2 == 5){
        topScore1 = (int) 6; 
      }
      
      //checks for threes
      if (count3 == 3 || count3 == 4 || count3 == 5){
        topScore1 = (int) 9; 
      }
      
      //checks for fours
      if (count4 == 3 || count4 == 4 || count4 == 5){
        topScore1 = (int) 12; 
      }
      
      //checks for fives
      if (count5 == 3 || count5 == 4 || count5 == 5){
        topScore1 = (int) 15; 
      }
      
      //checks for sixes
      if (count6 == 3 || count6 == 4 || count6 == 5){
        topScore1 = (int) 18; 
      }
      
      int top = (topScore1 + sumTop);
     System.out.println("The initial top score is: " + top); //prints out the top score
      
      int topBonus = 0;
      if (top >= 63){
        topBonus = (int) 35;
      }
     
     int finalTop = (int) (top + topBonus);
      System.out.println("The top score with bonus is: " + finalTop); //prints out the top score with bonus
      
    int score1 = 0;  
    //checks if there are 5 of a kind and scores as 50 if so
    if (count1 == 5 || count2 == 5 || count3 == 5 || count4 == 5 || count5 == 5 || count6 == 5) {
     score1 = (int) 50;
   }
     
          //checks if there is a sequence of 5 and scores as 40 if there is
          if (count1 == 1 && count2 == 1 && count3 == 1 && count4 == 1 && count5 == 1 || count2 == 1 && count3 == 1 && count4 == 1 && count5 == 1 && count6 == 1) {
     score1 = (int) 40;
   }
     
          //checks if there is a sequence of 4 and scores as 30 if there is
          if (count1 == 1 && count2 == 1 && count3 == 1 && count4 == 1 || count2 == 1 && count3 == 1 && count4 == 1 && count5 == 1 || count3 == 1 && count4 == 1 && count5 == 1 && count6 == 1){
     score1 = (int) 30;
   }
    
          
           //checks if there is a full house and scores 25 if so
         if (count1 == 2 && count2 == 3 || count1 == 2 && count3 == 3 || count1 == 2 && count4 == 3 || count1 == 2 && count5 == 3 || count1 == 2 && count6 == 3 || count2 == 2 && count1 == 3 || count2 == 2 && count3 == 3 || count2 == 2 && count4 == 3 || count2 == 2 && count5 == 3 || count2 == 2 && count6 == 3 || count3 == 2 && count1 == 3 || count3 == 2 && count2 == 3 || count3 == 2 && count4 == 3 || count3 == 2 && count5 == 3 || count3 == 2 && count6 == 3 || count4 == 2 && count2 == 3 || count4 == 2 && count3 == 3 || count4 == 2 && count1 == 3 || count4 == 2 && count5 == 3 || count4 == 2 && count6 == 3 || count5 == 2 && count2 == 3 || count5 == 2 && count3 == 3 || count5 == 2 && count4 == 3 || count5 == 2 && count1 == 3 || count5 == 2 && count6 == 3 || count6 == 2 && count2 == 3 || count6 == 2 && count3 == 3 || count6 == 2 && count4 == 3 || count6 == 2 && count5 == 3 || count6 == 2 && count1 == 3) {
      score1 = (int) 25;
   }
    
               //checks if there are 4 of a kind and sums the die if so
    if (count1 == 4 || count2 == 4 || count3 == 4 || count4 == 4 || count5 == 4 || count6 == 4) {
     score1 = (int) 50;
   }
          
          //checks if there are 3 of a kind and sums the die if so
   if (count1 == 3 || count2 == 3 || count3 == 3 || count4 == 3 || count5 == 3 || count6 == 3){
     int sum1 = (dice1 + dice2 + dice3 + dice4 + dice5);
   }
       System.out.println("the bottom score is " + score1);
      
       int totalScore = (int) (score1 + finalTop);
    System.out.println("the total score is " + totalScore);
 
 
            
      
          
      
    } //end of main method
 } //end of class
    