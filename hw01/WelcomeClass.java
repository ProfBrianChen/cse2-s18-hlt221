//////////////////////////////////////////////
///CSE 2 Welcome Class
///
public class WelcomeClass {
  
    public static void main(String[] args) {
      // Prints "Welcome, Class" to the terminal window.
      System.out.println("   -----------");
      System.out.println("   | WELCOME |");
      System.out.println("   -----------");
      System.out.println("   ^  ^  ^  ^  ^  ^");
      System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\"); 
      System.out.println(" <-H--L--T--2--2--1->");
      System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("   v  v  v  v  v  v");
      
    }

}