//// Hayden Trautmann
// 2/11/18
// CSE2 hw03
//Write a program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid.

import java.util.Scanner; //imports the scanner 
public class Pyramid {
  
    public static void main(String[] args) {
      
    Scanner scan = new Scanner(System.in); //creates a new scanner
    System.out.println("The square side of the pyramid is: "); //user inputs the square side of the pyramid
      
    double squareSide = scan.nextDouble(); //creates scanner squareside
    System.out.println("The height of the pyramid is: "); //user inputs the height of the pyramid
   
    double height = scan.nextDouble(); //creates scanner height
    double squareSideSquared = Math.pow(squareSide, 2); //squares the square side
    double x = 3; 
    double volume; 
    volume = (height * squareSideSquared) / x; //volume of pyramid formula 
     
      System.out.println("The volume inside the pyramid is " + volume); //prints the volume
      
    }
}
      