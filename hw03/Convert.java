// Hayden Trautmann
// 2/11/18
// CSE2 hw03
//Hurricanes drop a tremendous amount of water on areas of land. 
//Write a program that asks the user for doubles that represent the number of acres of land
//affected by hurricane precipitation and how many inches of rain were dropped on average.  
//Convert the quantity of rain into cubic miles. Save the program as 


import java.util.Scanner; //imports the java scanner 
public class Convert {
  
    public static void main(String[] args) {
      
      Scanner scan = new Scanner(System.in); //creates a new scanner 
      
      System.out.println("Enter the affected area in acres: "); //user inputs the affected area in acres
      
     
      double input = scan.nextDouble(); //creates scan double 
      double acresToMilesSquared = 0.0015625;
      double areaInMilesSquared;
      areaInMilesSquared = input * acresToMilesSquared; //converts acres of rain to miles squared 
      
      System.out.println("Enter the rainfall in the affected area: "); //user inputs the rainfall in the affected area
      double rainInches = scan.nextDouble();
      double inchesToMiles = 0.0000157828;
      double milesOfRain;
      milesOfRain = rainInches * inchesToMiles; //converts inches of rain to miles
      
      double cubicMilesOfRain; 
      cubicMilesOfRain = milesOfRain * areaInMilesSquared; //multiplies miles of rain by miles squared of rain to get cubic miles of rain
      
      System.out.println("There were " + cubicMilesOfRain + " cubic miles of rain."); //prints the rain in cubic miles
     

    }
}
