// Hayden Trautmann
// 1/2/18
// CSE2 lab02 
// print the number of minutes for each trip
// print the number of counts for each trip
// print the distance of each trip in miles
// print the distance for the two trips combined

public class Cyclometer {
    	// main method 
   	public static void main(String[] args) {

       int secsTrip1=480;  // seconds elapsed during trip 1
       int secsTrip2=3220;  // seconds elapsed during trip 2
		   int countsTrip1=1561;  // counts ( wheel rotations) in trip 1
		   int countsTrip2=9037; // counts (wheel rotations) in trip 2
      
    double wheelDiameter=27.0,  // bike wheel diameter
  	PI=3.14159, // pi needed to calculate circumference
  	feetPerMile=5280,  // feet to mile conversion
  	inchesPerFoot=12,   // inches to feet conversion
  	secondsPerMinute=60;  // seconds to minute conversion
	  double distanceTrip1, distanceTrip2,totalDistance;  // double that stores each distance and the total distance
      
        System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      
      	//run the calculations; store the values. Document your
		//calculation here. What are you calculating?
		// calculating the circumference of the wheel then multiplying it by the number of rotations the wheel had to find the distance1 in inches
		// convert the inches to to feet, then feet to miles to get the distance of trip 1 in miles. 
    // does the same for trip 2 then sums the two distances to get the total distance
      
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
	
      //Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
      
      
 




      
  

	}  //end of main method   
} //end of class
