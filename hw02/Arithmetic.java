// Hayden Trautmann
// 1/5/18
// CSE2 hw02
//Total cost of each kind of item (i.e. total cost of pants, etc)
//Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
//Total cost of purchases (before tax)
//Total sales tax
//Total paid for this transaction, including sales tax. 

public class Arithmetic {
  
    public static void main(String[] args) {


//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

double totalCostOfPants =  (numPants * pantsPrice);   //total cost of pants

double totalCostOfShirts =  (numShirts * shirtPrice);   //total cost of shirts

double totalCostOfBelts = (numBelts * beltCost);   //total cost of belts

      System.out.println("the total cost of pants is $" + totalCostOfPants); //prints total cost of pants
      System.out.println("the total cost of shirts is $" + totalCostOfShirts); //prints total cost of shirts
      System.out.println("the total cost of belts is $" + totalCostOfBelts);  //prints total cost of belts
     
     double SalesTaxOnPants =  ((int) ( (totalCostOfPants * paSalesTax) * 100)  /100.0); //total tax on pants
     double SalesTaxOnShirts =  ((int) ( (totalCostOfShirts * paSalesTax) * 100)  /100.0); //total tax on shirts
     double SalesTaxOnBelts =  ((int) ( (totalCostOfBelts * paSalesTax) * 100)  /100.0); //total tax on belts
       
     System.out.println("the total cost of sales tax on pants is $" + SalesTaxOnPants); //prints sales tax on pants
      System.out.println("the total cost of sales tax on shirts is $" + SalesTaxOnShirts); //prints sales tax on shirts
       System.out.println("the total cost of sales tax on belts is $" + SalesTaxOnBelts); //prints sales tax on belts
     
     //prints the total cost of purchases
     System.out.println("the total cost of the purchases is $" + ((totalCostOfPants + totalCostOfShirts) + totalCostOfBelts)); 
     
     //total sales tax 
     double totalCostOfPASalesTax = ((SalesTaxOnBelts + SalesTaxOnPants) + SalesTaxOnShirts);
    
     //prints the total cost of sales tax
     System.out.println("the total cost of sales tax is $" + totalCostOfPASalesTax);
     //prints the total cost including sales tax
     System.out.println("the total cost including sales tax is $" + (totalCostOfPASalesTax + totalCostOfPants + totalCostOfShirts + totalCostOfBelts)); 
                       

} //end of main method 
  } //end of class
