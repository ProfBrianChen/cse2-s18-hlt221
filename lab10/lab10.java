//Hayden Trautmann
// 4/22/18
//lab10 

import java.util.Scanner;
public class lab10{
 
    public static void main(String[] args){
    Scanner s = new Scanner(System.in);
    System.out.println("if you want a row major, enter true, if you want a column major enter false");
     boolean format = s.nextBoolean();
      
    System.out.println("Enter width of the array");
    int width = s.nextInt();
    System.out.println("Enter height of the array");
    int height = s.nextInt();
      System.out.println("  ");
  int[][] newArray = increasingMatrix(width, height, format); 
      
     //prints out the matrix
      printMatrix(newArray, format);
      System.out.println("  ");
       
      int[][] transArray = new int[width][height];
      //if the array is column major, it is translated into row major
      if(format == false){
     transArray  =  translate(newArray, height, width);
      format = true;
      printMatrix(transArray, format);
   }
      
      //adds the matrices if the height and width are equal, if not it retruns null
      addMatrix(newArray, transArray, height, width);
     
      
      
    } //end of main method
      
      
      
      //method that returns a collumn or row major array
    public static int[][] increasingMatrix(int width, int height, boolean format){
    
    
      
      if(format == false){
   //col major
    int[][] colMajor = new int[height][width];
   int z = 0;
    for(int i=0; i<height; i++){ 
    for(int j = 0; j<width; j++){
      colMajor[i][j] = z;
      z++;
       }
     }
        return colMajor;
    }
    
      
      //row major
     else{
    int[][] rowMajor = new int[width][height];
    int w = 0;
    for(int k=0; k<width; k++){ 
    for(int f = 0; f<height; f++){
      rowMajor[k][f] = w;
      w++;
      }
    }
       return rowMajor;
  } 
    
  }//end of method

 public static void printMatrix( int[][] array, boolean format ){
  
   if(format == true){
   //row major print
      for(int k=0; k < array.length; k++){ 
    for(int f = 0; f< array[k].length; f++){
      System.out.print(array[k][f] + " ");
    }
        System.out.println();
      }
   }
  
  else{
  
   //collumn major
      for(int i=0; i < array[0].length; i++){ 
    for(int j = 0; j< array.length; j++){
      System.out.print(array[i][j] + " ");
    }
        System.out.println();
      }

  }
 }
  
  
  //translates the array into row major if collumn major
  public static int[][] translate(int[][] array, int height, int width){
    
    
    int[][] newArray = new int[height][width];
   int z = 0;
    for(int i=0; i<width; i++){ 
    for(int j = 0; j<height; j++){
         newArray[j][i] = array[i][j];
      z++;
       }
     }
        return newArray;
    }
  
  //adds matrices together
 public static void addMatrix( int[][] newArray, int[][] transArray, int height, int width){
   if(height == width){
     int[][] addMatrix = new int[height][width];
   int z = 0;
    for(int i=0; i<width; i++){ 
    for(int j = 0; j<height; j++){
         addMatrix[j][i] = transArray[i][j] + newArray[i][j];
      z++;
       }
     }
        // prints out addMatrix
   boolean format = true;
     printMatrix(addMatrix, format);
     
   }
  //if the arrays have different heights and widths and cannot be added together, prints "the arrays cannot be added!" 
  else{
    System.out.println("The arrays cannot be added!");
  }
 }//end of addMatrix
  } //end of class
 
  


