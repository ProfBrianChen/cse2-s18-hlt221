// Hayden Trautmann
// 4/8/18
// CSE2 hw08
import java.util.Random;
import java.util.Scanner;
public class CSE2Linear{
  

  
public static void main(String[] args){
 
  int size = 15;
  
int[] a = new int[15];
  
  int grade = 0;
  int i = 0;
  int scramble;
  int n;
  
  Scanner s = new Scanner(System.in);
  
  while (i < 15){
  System.out.println("Enter 15 ascending ints for final grades in CSE2 from 0 to 100:");
 boolean check = s.hasNextInt();
    if (check = false){
     System.out.println("Error: must enter an int");
     continue;
    }
    grade = s.nextInt();
  if (grade < 0 || grade > 100){
     System.out.println("Error: must enter an int from 0 to 100");
    continue;
  } // case for first entered value
    if(i == 0){
      a[i] = grade;
      i++;
      continue;
    }
     a[i] = grade;
     if (a[i] > a[i-1]){
      i++;
       if(i == 14){
          System.out.println(a[i]);
       break;
       }
      continue;
    }
     System.out.println("Error: must be greater than the previous grade");
continue; 
  }
    System.out.println("Enter a grade to search for: ");
    int searchVal = s.nextInt();
 
  lsearch(a,searchVal);
  scramble(a);
  for( i = 0; i < size; i++){
    System.out.print(a[i] + " ");
  }
  System.out.println();
  bsearch(a,searchVal);
} 
   
  
 //scramble method that scrambles the grades 
  public static int[] scramble(int[] b){
    int size = b.length;
    for(int i = 0; i < size; i++){
       int rand = (int) (Math.random() * size);
       int temp = b[i];
      b[i] = b[rand];
      b[rand] = temp;
    }
    
    return b;
   
    }
  
  
  //method lsearch asks user to input a certain number and linearly searches the array for it
  public static void lsearch(int[] c, int searchVal){
   int size = c.length;
    boolean find;
    find = false;
    for( int i = 0; i < size; i++){
    //  int search = c[i];
     // c[i] = c[n];
     // c[n] = search;
      if(c[i] == searchVal){
        System.out.println(searchVal + " was found at iteration " + (i + 1));
       find = true;
        break; 
      }
      
      }
    if(find == false){
    System.out.println(searchVal + " was not found after " + (15) + " iterations"); 
    }
  }
   
  
  
  //method bsearch asks user to input a certain number and binarily searches the array for it
  public static void bsearch(int[] c, int searchVal){ 
   
    int low = 0;
    int high = c.length - 1;
    int mid;
    while (low < high){
        mid = ((high - low) / 2) + low;
      if (c[mid] == searchVal){
        System.out.println("found " + searchVal);
          break;
    }
     if(searchVal > c[mid] ){
       System.out.println(" searching the upper half");
       low = mid + 1;
     }
      if(searchVal < c[mid] ){
         System.out.println(" searching the lower half");
      high  = mid - 1;
     }    
    }          
   }     
    

    
  
  }
  
