// Hayden Trautmann
// 4/8/18
// CSE2 hw08 part 2

  import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }


  
  
//   method that returns a random input into an array
  public static int[] randomInput(){
   
   int[] newArray1 = new int[10];
    for(int i = 0; i < 10; i++){
       int n = (int) (Math.random() * 10);
       newArray1[i] = n;
     // b[i] = b[n];
     // b[n] = scramble;
    }
    return newArray1; 
  
  }
 
//   method that deletes a number from the aray
  public static int[] delete(int[] b, int index){
		int size = b.length;
 int[] newArray = new int[size - 1];
		for(int i = 0; i < size; i++){
			if(i < index){
				newArray[i] = b[i];
			}
			else if(i > index){
				newArray[i - 1] = b[i];
				
			}
				
		
			}
	return newArray;

  }
	
	
 public static int[] remove(int[] b, int target){
		int size = b.length;
	int count = 0;
	int[] newInt = new int[size];
	 for(int i = 0; i < size; i++){
		if(b[i] == target){
			count ++;
			newInt[i] = 1;
		}
	}
	 int[] result = new int[size - count];
	 int temp = count;
for(int i = 0; i < size; i++){
	if(newInt[i] != 1){
		
		result[i - (temp - count)] = b[i];
	}
	if (newInt[i] == 1) {
		count--;
	}
  }
	
	return result;
	
}
	
}
  
  
  
  


 
